#ifndef COOLAPPLICATION_IAPPLICATION_H
#define COOLAPPLICATION_IAPPLICATION_H 1

// Header was moved to CoolKernel in COOL 2.6.0 to remove cyclic dependencies
// The CoolApplication copy is kept for backward compatibility in the user API
#include "CoolKernel/IApplication.h"

#endif // COOLAPPLICATION_IAPPLICATION_H
