#!/usr/bin/env python

import unittest, sys
from PyCool import cool, coral, is_64bits

class TestBlob( unittest.TestCase ):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def _test_005_typedef(self):
        """Check cool.Blob64k typedef"""
        self.assertTrue(cool.Blob64k == coral.Blob)
        self.assertTrue(dir(cool.Blob64k) == dir(coral.Blob))

    def _test_006_typedef(self):
        """Check cool.Blob16M typedef"""
        self.assertTrue(cool.Blob16M == coral.Blob)
        self.assertTrue(dir(cool.Blob16M) == dir(coral.Blob))

    def _test_007_typedef(self):
        """Check cool.Blob128M typedef"""
        self.assertTrue(cool.Blob128M == coral.Blob)
        self.assertTrue(dir(cool.Blob128M) == dir(coral.Blob))

    def test_010_init(self):
        """Initialization"""
        blob = coral.Blob()
        self.assertTrue( not blob is None )

        N = 100
        blob = coral.Blob(N)
        self.assertTrue( not blob is None and blob.size() == N )

    def test_015_len(self):
        """Operator __len__"""
        N = 100
        blob = coral.Blob(N)
        self.assertTrue( blob.size() == len(blob) )

    def test_020_access(self):
        """Access bytes"""
        N = 100
        blob = coral.Blob(N)
        for i in range(N):
            blob[i] = i%256
        for i in range(N):
            self.assertTrue(blob[i] == i%256)

    def test_030_iterator(self):
        """Iterate over bytes"""
        N = 100
        blob = coral.Blob(N)
        for i in range(N):
            blob[i] = i%256
        i = 0
        for j in blob:
            self.assertTrue(j == i)
            i += 1

    def test_040_file_interface(self):
        """File-like interface"""
        blob = coral.Blob()

        blob.write('0123456789')
        blob.write('abcdefghij')
        self.assertTrue( 20 == len(blob) )

        blob.seek(0)
        self.assertTrue( "01234" == blob.read(5).decode() )

        blob.seek(5,1)
        print(blob.pos)
        self.assertTrue( "abcde" == blob.read(5).decode() )

        blob.seek(-3,2)

        self.assertTrue( "hij" == blob.read().decode() )

        blob.seek(13)
        self.assertTrue( 13 == blob.tell() )

if __name__ == '__main__':
    unittest.main( testRunner =
                   unittest.TextTestRunner(stream=sys.stdout,verbosity=2) )

