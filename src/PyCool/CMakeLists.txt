# Required external packages
if (LCG_python3 STREQUAL on) # CORALCOOL-2976
  find_package(PythonLibs REQUIRED 3)
else()
  find_package(PythonLibs REQUIRED)
endif()
include_directories(${PYTHON_INCLUDE_DIRS})
find_package(ROOT) # Really REQUIRED, but avoid blocking (CORALCOOL-2813)

# Make PyCool_helpers.h visible verbatim in PyCoolDict.so (bug #103539)
# See http://stackoverflow.com/questions/13470499
file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/python/PyCool/_internal)
add_custom_command(OUTPUT ${CMAKE_BINARY_DIR}/python/PyCool/_internal/PyCool_headers_and_helpers.h COMMAND cat ${CMAKE_CURRENT_SOURCE_DIR}/../RelationalCool/src/PyCool_helpers.h ${CMAKE_CURRENT_SOURCE_DIR}/dict/PyCool_headers.h > ${CMAKE_BINARY_DIR}/python/PyCool/_internal/PyCool_headers_and_helpers.h DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/../RelationalCool/src/PyCool_helpers.h ${CMAKE_CURRENT_SOURCE_DIR}/dict/PyCool_headers.h)
add_custom_target(PyCool_headers_and_helpers_h ALL DEPENDS ${CMAKE_BINARY_DIR}/python/PyCool/_internal/PyCool_headers_and_helpers.h)
install(FILES ${CMAKE_BINARY_DIR}/python/PyCool/_internal/PyCool_headers_and_helpers.h DESTINATION python/PyCool/_internal)

# Copy and install python modules into the build and install areas
include(CORALConfigPython)
coral_install_python_modules()

# Copy and install python tests into the build and install areas
include(CORALConfigScripts)
copy_and_install_python_test(test_ChannelSelection.py)
copy_and_install_python_test(test_Channels.py)
copy_and_install_python_test(test_CoolFunctionality.py)
copy_and_install_python_test(test_coolBlob.py)
copy_and_install_python_test(test_coralAttributeList.py)
copy_and_install_python_test(test_coralBlob.py)
copy_and_install_python_test(test_FolderSpecification.py)
copy_and_install_python_test(test_IDatabase.py)
copy_and_install_python_test(test_IDatabaseSvc.py)
copy_and_install_python_test(test_IFolder.py)
copy_and_install_python_test(test_IFolderSet.py)
copy_and_install_python_test(test_IObjectIterator.py)
copy_and_install_python_test(test_IObject.py)
copy_and_install_python_test(test_RecordSpecification.py)

# Set ROOT environment variables (from cmake variables set in FindROOT.cmake)
if (ROOT_FOUND)
  coral_build_and_release_env(SET ROOTSYS ${ROOTSYS})
  coral_build_and_release_env(APPEND PYTHONPATH ${ROOT_LIBRARY_DIRS})
  find_path(_manpath man1 HINTS ${ROOTSYS}/man NO_DEFAULT_PATH)
  if(_manpath)
    coral_build_and_release_env(APPEND MANPATH ${_manpath})
  endif()
endif()

# Test the compilation of PyCool_headers.h
include_directories(${ROOT_INCLUDE_DIRS})
coral_add_unit_test(PyCoolHeaders LIBS lcg_CoolKernel)
