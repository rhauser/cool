# See also test_root8458.py (and CORALCOOL-2959)
import sys
if len(sys.argv)!=2 or ( sys.argv[1]!='crash' and sys.argv[1]!='nocrash' ):
    # NB: 'crash' causes a crash, 'nocrash' does not cause a crash
    print "Usage: python", sys.argv[0], "<crash|nocrash>"
    sys.exit(0)
if sys.argv[1]=='crash': crash=True
else: crash=False
print "Your choice: CRASH =",crash
#crash=True

# Import PyCool
###from PyCool import cool

# Import "mini-PyCool"
# Instantiate FieldSelection excluding duplicates as in INST_COOL_IFIELD_FUNCT
# Use type_info.name() as variable name to keep track of this more easily...
import cppyy
cppyy.gbl.gSystem.Load('liblcg_CoolApplication.so')
cppyy.gbl.gInterpreter.ProcessLine('#include "CoolKernel/FieldSelection.h"')
if crash: cppyy.gbl.gInterpreter.ProcessLine("#define CRASH 1")
cppyy.gbl.gInterpreter.ProcessLine("""
  template cool::FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Bool& b );
  template cool::FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::UChar& h );
  template cool::FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Int16& s );
  template cool::FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::UInt16& t );
  template cool::FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Int32& i );
  template cool::FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::UInt32& j );
  template cool::FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Int64& x );
  template cool::FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::UInt64& y );
  template cool::FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Float& f );
  //=== WHEN a cout is included in FieldSelectil ctor, THEN:
  // - including this below causes this test NOT to throw and abort
  // - commenting out this line below causes this test to throw and abort
#ifndef CRASH
#pragma message(\"WILL NOT CRASH\")
  template cool::FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Double& d );
#else
#pragma message(\"WILL CRASH\")
#endif
  //=== These three below are always irrelevant and I keep them out...
  //template cool::FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::String255& Ss );
  //template cool::FieldSelection::FieldSelection( const std::string& name, const StorageType::TypeId typeId, Relation relation, const cool::Blob64k& N5coral4BlobE );
""")
cool = cppyy.gbl.cool

# Test (as in test_root6068.py, unstable vs crash/nocrash)
cool.FieldSelection("i",cool.StorageType.Int32,cool.FieldSelection.EQ,10)

# Test (as in the original test_root8458.py, no instability observed)
#cool.FieldSelection("x",cool.StorageType.Bool,cool.FieldSelection.EQ,True)
#cool.FieldSelection("x",cool.StorageType.Bool,cool.FieldSelection.EQ,10)
#cool.FieldSelection("x",cool.StorageType.Bool,cool.FieldSelection.EQ,10.)

# Other tests
###cool.FieldSelection("x",cool.StorageType.String255,cool.FieldSelection.EQ,"aaa")
###cool.FieldSelection("x",cool.StorageType.String255,cool.FieldSelection.EQ,cppyy.gbl.coral.Blob())
