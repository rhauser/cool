#ifndef COOLKERNEL_FIELDSELECTION4_H
#define COOLKERNEL_FIELDSELECTION4_H 1

#include <iostream> // Debug ROOT-8458
#include "CoolKernel/FieldSpecification.h"
#include "CoolKernel/IRecordSelection.h"
#include "CoolKernel/Record.h"

namespace cool
{
  class FieldSelection4 : virtual public IRecordSelection
  {
  public:
    enum Relation { EQ, NE, GT, GE, LT, LE };
    enum Nullness { IS_NULL, IS_NOT_NULL };
    virtual ~FieldSelection4(){}
    template<typename T> FieldSelection4( const std::string& name,
                                          const StorageType::TypeId typeId,
                                          Relation relation,
                                          const T& refValue );
    // IRecordSelection interface
    bool canSelect( const IRecordSpecification& ) const{ return true; }
    bool select( const IRecord& ) const{ return true; }
    IRecordSelection* clone() const{ return nullptr; }
  private:
    FieldSelection4();
    FieldSelection4( const FieldSelection4& rhs );
    FieldSelection4& operator=( const FieldSelection4& rhs );
  private:
    Record m_refValue;
    Relation m_relation;
  };

  //---------------------------------------------------------------------------

  template<typename T>
  inline FieldSelection4::FieldSelection4( const std::string& name,
                                           const StorageType::TypeId typeId,
                                           Relation relation,
                                           const T& refValue )
    : m_refValue( FieldSpecification( name, typeId ) )
    , m_relation( relation )
  {
    std::cout << "FieldSelection for StorageType=" << typeId << "(" << StorageType::storageType(typeId).name() << "," << StorageType::storageType(typeId).cppType().name() << ") against " << name << "(" << typeid(T).name() << ")" << std::endl;
    m_refValue[0].setValue( refValue );
  }

  //---------------------------------------------------------------------------

  // Template instantiation
  template FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const bool& refValue );
  template FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const int& refValue );
  template FieldSelection4::FieldSelection4( const std::string& name, const StorageType::TypeId typeId, Relation relation, const float& refValue );

}
#endif // COOLKERNEL_FIELDSELECTION4_H
