# See also test_root6068.py
import os
dir=os.path.dirname( os.path.realpath(__file__) )
os.system("cd "+dir+"; $CXX -std=c++1y -pedantic -Wall -Wextra -Wno-long-long -Wno-deprecated -pthread -fPIC -Wl,--no-undefined -g -fPIC -o test_root8458b.o -c test_root8458b.cpp; $CXX -fPIC -std=c++1y -pedantic -Wall -Wextra -Wno-long-long -Wno-deprecated -pthread -fPIC -Wl,--no-undefined -g   -shared -Wl,-soname,test_root8458b.so -o test_root8458b.so test_root8458b.o")
import ROOT
ROOT.gSystem.Load(dir+"/test_root8458b.so")
ROOT.gInterpreter.ProcessLine("#include \""+dir+"/test_root8458b.h\"")

cool = ROOT.cool
for typeId in cool.StorageType.Bool, cool.StorageType.Int32, cool.StorageType.UInt32, cool.StorageType.Float: #, cool.StorageType.Double:
    print "================================"
    typeName=cool.typeIdName[typeId]
    print typeId, typeName
    for refValue in True, 10, 10.1: #, "string":
        try:
            print "TRY FS ctor for "+typeName+" against refValue="+str(refValue)
            fs = cool.FieldSelection("x",typeId,cool.FieldSelection.EQ,refValue)
            print "OK!"
        ###except Exception,e: print "FAILED!", e
        except: print "FAILED!"
        print
