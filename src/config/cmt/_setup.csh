
if ( "$*" != "" ) then
  echo "ERROR! Unexpected parameters '$*'" > /dev/stderr
  exit 1
endif

# determine the path to this sourced script
# (installed into TOP/<tag>/bin from the stub in TOP/<src>/config/cmt)
set source=($_)
if ( "${source[1]}" != "source" || "${source[2]}" == "" ) then
  echo "ERROR! The setup script was not sourced? ('$source')" > /dev/stderr
  exit 1
endif
if ( "${source[3]}" != "" ) then
  echo "ERROR! Unexpected parameters ('$source')" > /dev/stderr
  exit 1
endif
set source=`dirname ${source[2]}`
set source=`cd ${source}; pwd`
if ( `basename ${source}` == "cmt" ) then
  # original location TOP/<src>/config/cmt
  # configure from config/cmt
  set config=config
else
  # installed location TOP/<tag>/bin
  # configure from CoralTest/cmt or CoolTest/cmt
  set source=`cd ${source}/..; pwd`
  if ( -d ${source}/CoralTest/cmt ) then
    set source=${source}/CoralTest/cmt
    set config=CoralTest
  else if ( -d ${source}/CoolTest/cmt ) then
    set source=${source}/CoolTest/cmt
    set config=CoolTest
  else
    echo "ERROR! Neither CoralTest/cmt nor CoolTest/cmt was found in ${source}"
    exit 1
  endif
endif
echo "Setting config in ${source} for ${CMTCONFIG}"
pushd ${source} > /dev/null

# Case 1 : setup from config/cmt
if ( "$config" == "config" ) then
  source CMT_env.csh > /dev/null
  if ( "$status" != "0" ) then
    echo "ERROR! CMT_env.csh failed" > /dev/stderr
    popd > /dev/null
    exit 1
  endif
  if ( $?CMTROOT == 0 ) then
    echo "ERROR! CMTROOT is not set?" > /dev/stderr
    popd > /dev/null
    exit 1
  endif
  source ${CMTROOT}/mgr/setup.csh
# Case 2 : setup from CoralTest/cmt or CoolTest/cmt
else
  setenv CMTVERS v1r20p20090520
  setenv CMTROOT /afs/cern.ch/sw/contrib/CMT/$CMTVERS
  setenv CMTSITE CERN
  setenv SITEROOT /afs/cern.ch
  setenv CMTPROJECTPATH ${source}/../../../../..:$SITEROOT/sw/lcg/releases:$SITEROOT/sw/lcg/app/releases
  setenv CMTPATH
  unsetenv CMTEXTRATAGS
  unsetenv CMTUSERCONTEXT
  unsetenv CMTINSTALLAREA
  source ${CMTROOT}/mgr/setup.csh
endif

# Create and source setup from <config>/cmt 
set tempfile=`mktemp /tmp/tmp.$USER.XXXXXXXXXX`
${CMTROOT}/mgr/cmt setup -csh -pack=config -version=v1 -path=${source} -no_cleanup $* >${tempfile}
if ( "$status" != "0" ) then
  echo "ERROR! cmt setup failed" > /dev/stderr
  popd > /dev/null
  exit 1
endif
popd > /dev/null
source ${tempfile}
/bin/rm -f ${tempfile}
