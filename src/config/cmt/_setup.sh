
if [ "$*" != "" ]; then
  echo "ERROR! Unexpected parameters '$*'" > /dev/stderr
  return 1
fi

# determine the path to this sourced script
# (installed into TOP/<tag>/bin from the stub in TOP/<src>/config/cmt)
source=$BASH_SOURCE
if [ "$source" == "" ]; then
  echo "ERROR! The setup script was not sourced?" # > /dev/stderr
  return 1
fi
source=`dirname ${source}`
source=`cd ${source}; pwd`
if [ `basename ${source}` == "cmt" ]; then
  # original location TOP/<src>/config/cmt
  # configure from config/cmt
  config=config
else
  # installed location TOP/<tag>/bin
  # configure from CoralTest/cmt or CoolTest/cmt
  source=`cd ${source}/..; pwd`
  if [ -d ${source}/CoralTest/cmt ]; then
    source=${source}/CoralTest/cmt
    config=CoralTest
  elif [ -d ${source}/CoolTest/cmt ]; then
    source=${source}/CoolTest/cmt
    config=CoolTest
  else
    echo "ERROR! Neither CoralTest/cmt nor CoolTest/cmt was found in ${source}"
    return 1
  fi
fi
echo "Setting config in ${source} for ${CMTCONFIG}"
pushd ${source} > /dev/null

# Case 1 : setup from config/cmt
if [ "$config" == "config" ]; then
  source CMT_env.sh > /dev/null
  if [ "$?" != "0" ]; then
    echo "ERROR! CMT_env.sh failed" > /dev/stderr
    popd > /dev/null
    return 1
  fi
  if [ "$CMTROOT" == "" ]; then
    echo "ERROR! CMTROOT is not set?" > /dev/stderr
    popd > /dev/null
    return 1
  fi
  source ${CMTROOT}/mgr/setup.sh
# Case 2 : setup from CoralTest/cmt or CoolTest/cmt
else
  export CMTVERS=v1r20p20090520
  export CMTROOT=/afs/cern.ch/sw/contrib/CMT/$CMTVERS
  export CMTSITE=CERN
  export SITEROOT=/afs/cern.ch
  export CMTPROJECTPATH=${source}/../../../../..:$SITEROOT/sw/lcg/releases:$SITEROOT/sw/lcg/app/releases
  unset CMTPATH
  unset CMTEXTRATAGS
  unset CMTUSERCONTEXT
  unset CMTINSTALLAREA
  source ${CMTROOT}/mgr/setup.csh
fi

# Create and source setup from <config>/cmt 
tempfile=`mktemp /tmp/tmp.$USER.XXXXXXXXXX`
${CMTROOT}/mgr/cmt setup -sh -pack=config -version=v1 -path=${source} -no_cleanup $* >${tempfile}
if [ "$?" != "0" ]; then
  echo "ERROR! cmt setup failed" > /dev/stderr
  popd > /dev/null
  return 1
fi
popd > /dev/null
source ${tempfile}
/bin/rm -f ${tempfile}
