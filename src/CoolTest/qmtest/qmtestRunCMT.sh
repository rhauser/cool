#! /bin/bash

# Go to the qmtest directory
cd `dirname ${0}`
theQmtDir=`pwd`

# Run through valgrind if the first argument is '-valgrind'
if [ "$1" == "-valgrind" ] || [ "$1" == "--valgrind" ]; then
  echo "The test suite will be executed through valgrind ('-valgrind')"
  export CORAL_TESTSUITE_VALGRIND=1
  shift
else
  echo "The test suite will NOT be executed through valgrind (no '-valgrind')"
  unset CORAL_TESTSUITE_VALGRIND
fi

# Check the input arguments to choose which tests to run
if [ "$1" = "" ]; then
    #--- All tests (whether in CoralTest or CoolTest)
    theTests=ALL 
    #--- Specific tests (only in CoralTest)
    ###theTests=integration
    ###theTests=unit
    ###theTests=unit_coralserver
    #--- Specific tests (only in CoolTest)
    ###theTests=sqlite
    ###theTests=relationalcool.oracle.raldatabasesvc
    ###theTests=relationalcool.mysql.raldatabasesvc
    ###theTests=relationalcool.sqlite.raldatabasesvc
    ###theTests=pycool.import_pycool
    ###theTests=pycoolutilities.sqlite.evolution_130
    ###theTests=pycoolutilities.frontier
else
    theTests=""
    while [ "$1" != "" ]; do
        if [ "$theTests" = "" ]; then
            theTests="$1"
        else
            theTests="$theTests $1"
        fi
        shift
    done
fi
echo Will launch \'qmtest run ${theTests}\'

# Check if this is CoralTest or CoolTest
pushd .. > /dev/null
if [ `basename ${PWD}` == "CoralTest" ]; then
  thisPkg=CoralTest
elif [ `basename ${PWD}` == "CoolTest" ]; then
  thisPkg=CoolTest
else
  echo "ERROR! Unknown directory ${PWD} is neither CoralTest nor CoolTest"
  exit 1
fi
popd > /dev/null

# Define location of log directory (now always local in ../../..)
# [Eventually local in ../../.. for own build, /tmp for release)
theLogParentDir=`cd ../../..; pwd`

# Check out from SVN the logs directory if it does not exist
# Stop if an older version of logs exist without qmtestCoral or qmtestCool
# Define the qmtest results file
pushd $theLogParentDir > /dev/null
if [ "$thisPkg" == "CoralTest" ]; then
  if [ ! -d logs ]; then
    echo "WARNING! Missing logs directory will be checked out from lcgcoral SVN"
    svn co svn+ssh://svn.cern.ch/reps/lcgcoral/logs
  fi
  if [ ! -d logs/qmtestCoral ]; then
    echo "ERROR! Directory ${theLogParentDir}/logs/qmtestCoral does not exist"
    exit 1
  fi
  theQmrDir=${theLogParentDir}/logs/qmtestCoral
else
  if [ ! -d logs ]; then
    echo "WARNING! Missing logs directory will be checked out from lcgcool SVN"
    svn co svn+ssh://svn.cern.ch/reps/lcgcool/logs
  fi
  if [ ! -d logs/qmtestCool ]; then
    echo "ERROR! Directory ${theLogParentDir}/logs/qmtestCool does not exist"
    exit 1
  fi
  theQmrDir=${theLogParentDir}/logs/qmtestCool
fi
popd > /dev/null

# Go to the cmt directory and setup cmt
pushd ../../config/cmt > /dev/null
if [ "$?" != "0" ]; then
  echo "ERROR! Could not cd ../../config/cmt"
  exit 1
fi
###echo "Set up the CMT runtime environment"
source CMT_env.sh > /dev/null
###echo "Set up the CORAL/COOL runtime environment"
source setup.sh
if [ "$?" != "0" ]; then
  echo "ERROR! Could not source setup.sh"
  exit 1
fi

# Set a few additional environment variables
# (as in prepare_env in test_functions.sh)
###export CORAL_AUTH_PATH=${HOME}/private
###export CORAL_DBLOOKUP_PATH=${HOME}/private

# Ignore QMTEST timeouts for COOL
export COOL_IGNORE_TIMEOUT=yes

# Go to the qmtest directory and check QMTEST_CLASS_PATH (bug #86964)
###cmt show macro CMTINSTALLAREA
###cmt show set QMTEST_CLASS_PATH
popd > /dev/null
echo "Using QMTEST_CLASS_PATH=$QMTEST_CLASS_PATH"
if [[ :$QMTEST_CLASS_PATH: != *:`pwd`:* ]] ; then
  echo "ERROR! Expected QMTEST_CLASS_PATH should contain "`pwd`
  exit 1
fi

# Check that valgrind is available if required
if [ "$CORAL_TESTSUITE_VALGRIND" == "1" ]; then
  which valgrind > /dev/null 2>&1
  if [ "$?" != "0" ]; then
    echo "ERROR! No path to valgrind"
    echo "PATH is:$PATH" | tr ":" "\n"
    exit 1
  fi
  # Fix "valgrind: failed to start tool 'memcheck' for platform 'amd64-linux'"
  # relocatability issues by setting VALGRIND_LIB (thanks to Rolf!)
  # [NB already set via CMT/CMake; redundantly set here for native valgrind use]
  binvalgrind=`which valgrind`
  binvalgrind=`dirname $binvalgrind`
  if [ ! -d $binvalgrind/../lib/valgrind ]; then
    echo "ERROR! Directory $binvalgrind/../lib/valgrind does not exist!"
    exit 1
  fi
  export VALGRIND_LIB=`cd $binvalgrind/../lib/valgrind; pwd`
  # Set the path to the suppression file (no longer set via CMT!)
  which coralValgrindWrapper.sh > /dev/null 2>&1
  if [ "$?" != "0" ]; then
    echo "ERROR! No path to coralValgrindWrapper.sh"
    echo "PATH is:$PATH" | tr ":" "\n"
    exit 1
  fi
  CORAL_TESTSUITE_VALGRIND_SUPP=`which coralValgrindWrapper.sh`
  CORAL_TESTSUITE_VALGRIND_SUPP=`dirname ${CORAL_TESTSUITE_VALGRIND_SUPP}`
  export CORAL_TESTSUITE_VALGRIND_SUPP=`cd ${CORAL_TESTSUITE_VALGRIND_SUPP}; pwd`/valgrind.supp
  if [ ! -f "${CORAL_TESTSUITE_VALGRIND_SUPP}" ]; then
    echo "ERROR! Suppression file not found: ${CORAL_TESTSUITE_VALGRIND_SUPP}" 
    exit 1
  fi
fi

# Run the tests from the qmtest directory
theQmr=${theQmrDir}/${CMTCONFIG}-cmt.qmr # Add "-cmt" (CORALCOOL-2842)
echo "Launch tests - results will be in ${theQmr}"
echo Launch \'qmtest run ${theTests}\'
###qmtest run -o ${theQmr} ${theTests} > /dev/null 2>&1
qmtest run -o ${theQmr} ${theTests} > /dev/null
qmtest summarize -f stats ${theQmr} ${theTests} | awk 'BEGIN{out=0}{if ($2=="STATISTICS") out=1; if (out==1) print}'

# Beautify and move the valgrind logs
if [ "$CORAL_TESTSUITE_VALGRIND" == "1" ]; then
  cd $theQmrDir/valgrind
  valfiles=`ls $CMTCONFIG/valgrind.*`
  if [ "$valfiles" == "" ]; then
    echo "WARNING! No valgrind logs found in $theQmrDir/valgrind/$CMTCONFIG/"
  else
    for valfile in $valfiles; do
      ###echo $valfile
      ${theQmtDir}/sedValgrindLog.sh $valfile
      mv $valfile .
    done
  fi
fi

# Do not attempt to run 'qmtest report/summarize' as "$?" is wrong on OSX?
