configure_file(qmtest/qmtestRun.sh.in ${CMAKE_BINARY_DIR}/qmtestRun.sh @ONLY)
execute_process(COMMAND chmod +x ${CMAKE_BINARY_DIR}/qmtestRun.sh)
install(PROGRAMS ${CMAKE_BINARY_DIR}/qmtestRun.sh DESTINATION .)

include(CORALConfigScripts)
file(GLOB _files RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/qmtest qmtest/QMTest/configuration qmtest/*.qms qmtest/*.qmt qmtest/*/*.qmt qmtest/*/*.qms qmtest/*/*/*.qmt qmtest/*/*/*.qms qmtest/*/*/*/*.qmt qmtest/*.qmc qmtest/COOLTests.py qmtest/text_result_stream.py qmtest/sedValgrindLog.sh)
foreach(_file ${_files})
  if(NOT IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/qmtest/${_file})
    copy_to_build(${_file} qmtest CoolTest/qmtest)
    get_filename_component(_dstdir CoolTest/qmtest/${_file} PATH)
    if(${_file} MATCHES ".sh")
      install(PROGRAMS ${CMAKE_BINARY_DIR}/CoolTest/qmtest/${_file} DESTINATION ${_dstdir})
    else()
      install(FILES ${CMAKE_BINARY_DIR}/CoolTest/qmtest/${_file} DESTINATION ${_dstdir})
    endif()
  endif()
endforeach(_file)

file(GLOB _files RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/qmtest/pycool.qms qmtest/pycool.qms/*.qmt.in)
foreach(_file import_cppyy.qmt import_root.qmt import_pycool.qmt)
  configure_file(qmtest/pycool.qms/${_file}.in ${CMAKE_BINARY_DIR}/CoolTest/qmtest/pycool.qms/${_file})
  install(FILES ${CMAKE_BINARY_DIR}/CoolTest/qmtest/pycool.qms/${_file} DESTINATION CoolTest/qmtest/pycool.qms/)
endforeach(_file)

# Workarounds to use the Python3 build of QMTest with Python2 (CORALCOOL-2954)
if (LCG_python3 STREQUAL on)
  find_package(QMTest) # ensure this is already called here
  configure_file(qmtest/qmtest.in ${CMAKE_BINARY_DIR}/CoolTest/bin/qmtest @ONLY)
  execute_process(COMMAND chmod +x ${CMAKE_BINARY_DIR}/CoolTest/bin/qmtest)
  install(PROGRAMS ${CMAKE_BINARY_DIR}/CoolTest/bin/qmtest DESTINATION CoolTest/bin)
  copy_and_install_file(sigmask.py qmtest CoolTest/bin CoolTest/bin)
endif()

#============================================================================
# RUNTIME ENVIRONMENT CONFIGURATION
#============================================================================

include(CORALEnvironmentHandling)

# coral_env_for_package(package)
function(coral_env_for_package _pkg)
  find_package(${_pkg})
  string(TOUPPER ${_pkg} _pkg_upper)
  if(${_pkg_upper}_FOUND)
    coral_build_and_release_env(PACKAGE ${_pkg})
  endif()
endfunction()

# Basic dependencies as in CoolKernel
coral_env_for_package(Boost)
coral_env_for_package(CORAL)

#----------------------------------------------------------------------------
# CORAL plugin runtime
#----------------------------------------------------------------------------

coral_env_for_package(XercesC)

coral_env_for_package(MySQL)
coral_env_for_package(SQLite)
coral_env_for_package(Frontier_Client)

# Workaround for CORALCOOL-2857: no Oracle on ARM
IF(NOT BINARY_TAG MATCHES "aarch64")
  coral_env_for_package(Oracle)
  # Runtime environment for CERN's Oracle databases
  # New deployment strategy for tnsnames.ora (CORALCOOL-2756, SPI-758, SPI-726).
  # Also workaround for obsolete tnsnames.ora in LHCb cvmfs (CORALCOOL-2164).
  # Disable Kerberos authentication on mac (CORALCOOL-2763 and CORALCOOL-1244).
  coral_build_and_release_env(SET NLS_LANG american_america.WE8ISO8859P1)
  coral_build_and_release_env(SET ORA_FPU_PRECISION EXTENDED)
  if(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    ###coral_build_and_release_env(SET TNS_ADMIN /afs/cern.ch/sw/lcg/app/releases/CORAL/internal/oracle/admin/adminNoKerberos) # See CORALCOOL-1244
    coral_build_and_release_env(DEFAULT TNS_ADMIN /afs/cern.ch/sw/lcg/app/releases/CORAL/internal/oracle/admin/adminNoKerberos) # See CORALCOOL-2890
  else()
    coral_build_and_release_env(DEFAULT TNS_ADMIN /eos/project-o/oracle/public/admin/) # See CORALCOOL-2956
  endif()
ENDIF()

#----------------------------------------------------------------------------
# Generic test runtime for CORAL and COOL
#----------------------------------------------------------------------------

coral_env_for_package(Valgrind)
coral_env_for_package(IgProf)
coral_env_for_package(gperftools)

#----------------------------------------------------------------------------
# More specific test runtime for COOL
#----------------------------------------------------------------------------

coral_env_for_package(vdt) #CORALCOOL-3030
coral_env_for_package(lzma) #CORALCOOL-3030

coral_build_env(PACKAGE QMTest SET QMTEST_CLASS_PATH ${CMAKE_BINARY_DIR}/CoolTest/qmtest)
coral_release_env(PACKAGE QMTest SET QMTEST_CLASS_PATH ${CMAKE_INSTALL_PREFIX}/CoolTest/qmtest)

# These are used within qmtest for logging (and configuring tests if needed)
# [Moved to CORALEnvironmentHandling.cmake for relocatability (CORALCOOL-2829)]
###coral_build_env(SET COOLSYS ${CMAKE_BINARY_DIR})
###coral_release_env(SET COOLSYS ${CMAKE_INSTALL_PREFIX})
get_filename_component(CORALSYS ${CORAL_LIBRARY_DIRS} PATH)
coral_build_and_release_env(SET CORALSYS ${CORALSYS})

# Workarounds to use the Python3 build of QMTest with Python2 (CORALCOOL-2954)
# NB This must come AFTER the QMTest environment setup so as to override it!
if (LCG_python3 STREQUAL on)
  if (BINARY_TAG MATCHES "slc6")
    coral_env_for_package(ffi) #CORALCOOL-3069
  endif()
  coral_build_env(PREPEND PATH ${CMAKE_BINARY_DIR}/CoolTest/bin)
  coral_release_env(PREPEND PATH ${CMAKE_INSTALL_PREFIX}/CoolTest/bin)
endif()

# Workaround for bug #40326 in SQLite tests
coral_build_and_release_env(SET CORAL_SQLITE_TEMP_STORE_MEMORY 1)

# Select from sys.xxx$ tables in CORAL data dictionary queries
# [done for avalassi, lcg_cool, lcg_cool_nightly even if this env is not set]
###coral_build_and_release_env(SET CORAL_ORA_SELECT_FROM_SYS_TABLES 1)

# Force immediate reload of short-lived cached queries (CORALCOOL-2421 and 2920)
coral_build_and_release_env(SET FRONTIER_FORCERELOAD short)
# Force immediate reload of all queries (workaround for Frontier bug #42465)
###coral_build_and_release_env(SET FRONTIER_FORCERELOAD long)

# Useful variables for COOL tests
coral_build_and_release_env(SET COOL_DISABLE_CORALCONNECTIONPOOLCLEANUP 1)
coral_build_and_release_env(SET COOL_ENABLE_COOLMSGREPORTER 1)
coral_build_and_release_env(DEFAULT COOL_MSGLEVEL Error) # Allow overloading! (CORALCOOL-2923)
###coral_build_and_release_env(DEFAULT COOL_MSGLEVEL Verbose)
###coral_build_and_release_env(SET COOL_QMTEST_SKIPEVOLVE 1)

# [NB Signal handlers for qmtest tests are defined in COOLTests.py too]
# Enable the COOL signal handler (disable the ROOT one in this case!)
###coral_build_and_release_env(SET COOL_ENABLE_COOLSIGNALHANDLER 1)

# Enable the new algorithm for MV bulk insertion (bug #17903)
# The same value would be used by default if this variable was not set
coral_build_and_release_env(SET COOL_MVINSERTION_PREFETCH_MAXROWS 1000)

# Workaround for ORA-01466 on Oracle 11g (bug #89735)
###coral_build_and_release_env(SET COOL_TESTSUITE_SLEEPFOR01466 1)

# Bypass tests that are expected to fail with ROOT6 (CORALCOOL-2741)
# The first two are no longer necessary as of ROOT 6.08 (ROOT-8175)
###coral_build_and_release_env(SET COOL_PYCOOLTEST_SKIP_EXCEPTIONS 1)
###coral_build_and_release_env(SET COOL_PYCOOLTEST_SKIP_ROOT6927 1)
# The third is still necessary for ROOT 6.08 (CORALCOOL-2959, ROOT-8458)
coral_build_and_release_env(SET COOL_PYCOOLTEST_SKIP_ROOT8458 1)

# Give release env for Qt5 (CORALCOOL-3006)
coral_build_and_release_env(SET QT_QPA_PLATFORM_PLUGIN_PATH $ENV{QT_PLUGIN_PATH})
#coral_build_and_release_env(SET QT_QPA_PLATFORM_PLUGIN_PATH /cvmfs/sft.cern.ch/lcg/releases/qt5/5.9.2-774b9/x86_64-slc6-gcc62-opt/plugins/platforms/)
coral_build_and_release_env(SET QT_XKB_CONFIG_ROOT /usr/share/X11/xkb)
#----------------------------------------------------------------------------
# Additional runtime for the COOL nightly tests
# Some settings are different on the various slots
#----------------------------------------------------------------------------

# Configure which tests are run by QMtest using COOL_QMTEST_TARGET
# [Allowed values: ALL, ALL_nomysql, ALL_sqlite]
# Run only SQLite by default (e.g. for test slots and new dev slots)
# Run all backends on the release, dev (preview) and dev1 (patches) slots
# In LCG_Builders/COOL/scripts/COOL_test.sh: 'qmtest run $COOL_QMTEST_TARGET'
# [TEMPORARY? no longer relevant after the move to run_nightly_tests?]
###set COOL_QMTEST_TARGET  ALL_sqlite \
###    lcg_ngt_slt_release ALL \
###    lcg_ngt_slt_dev     ALL \
###    lcg_ngt_slt_dev1    ALL
coral_build_and_release_env(SET COOL_QMTEST_TARGET ALL_sqlite)

