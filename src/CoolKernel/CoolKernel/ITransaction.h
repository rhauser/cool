#ifndef COOLKERNEL_ITRANSACTION_H
#define COOLKERNEL_ITRANSACTION_H 1

#include "CoolKernel/VersionInfo.h" // for #ifdef COOL4xx 

// This class only exists in the COOL400 API
#ifdef COOL400TX

namespace cool
{

  /** @file ITransaction.h
   *
   * Transaction interface for manual transaction handling.
   *
   * @author Sven A. Schmidt
   * @date 2008-06-29
   *///

  class ITransaction
  {

  public:

    virtual ~ITransaction() {}

    /// Commit the transaction
    virtual void commit() = 0;

    /// Rollback the transaction
    virtual void rollback() = 0;

  private:

    /// Assignment operator is private (see bug #95823)
    ITransaction& operator=( const ITransaction& rhs );

  };

}

#endif // COOL400

#endif // COOLKERNEL_ITRANSACTION_H
