#ifndef COOLKERNEL_FOLDERSPECIFICATION_H
#define COOLKERNEL_FOLDERSPECIFICATION_H 1

#include "CoolKernel/VersionInfo.h" // for #ifdef COOL4xx 

// Include files
#include "CoolKernel/IFolderSpecification.h"
#include "CoolKernel/RecordSpecification.h"

namespace cool
{

  //--------------------------------------------------------------------------

  /** @class FolderSpecification FolderSpecification.h
   *
   *  Specification of a COOL "folder".
   *  Concrete implementation of the IFolderSpecification interface.
   *
   *  This includes the payload specification and the versioning mode.
   *  The description is not included as it is a generic "HVS node"
   *  property (it applies to folder sets as well as to folders).
   *
   *  @author Andrea Valassi, Marco Clemencic and Martin Wache
   *  @date   2006-09-22
   *///

  class FolderSpecification : public IFolderSpecification
  {

  public:

    /// Destructor.
    virtual ~FolderSpecification();

    /// Constructor from payload specification (assuming SINGLE_VERSION versioning mode)
    FolderSpecification( const IRecordSpecification& payloadSpecification );

    /// Constructor from versioning mode and payload specification.
    /// [PayloadMode values are chosen for backward compatibility - bug #103351]
    /// Throws InvalidFolderSpecification if versioning mode is invalid.
    explicit
    FolderSpecification( FolderVersioning::Mode mode,
                         const IRecordSpecification& payloadSpecification,
#ifndef COOL400CPP11ENUM
                         PayloadMode::Mode payloadMode = PayloadMode::INLINEPAYLOAD
#else
                         PayloadMode::Mode payloadMode = PayloadMode::Mode::INLINEPAYLOAD
#endif
                         );

    /*
    /// Constructor from versioning mode and payload and channel specs.
    /// Throws InvalidFolderSpecification if versioning mode is invalid.
    FolderSpecification( FolderVersioning::Mode mode,
                         const IRecordSpecification& payloadSpecification,
                         const IRecordSpecification& channelSpecification );
    *///

    /// Copy constructor
    FolderSpecification( const FolderSpecification& rhs );

    /// Assignment operator
    FolderSpecification& operator=( const FolderSpecification& rhs );

    /// Get the versioning mode (const).
    const FolderVersioning::Mode& versioningMode() const;

    /// Get the payload specification (const).
    const IRecordSpecification& payloadSpecification() const;

    /*
    /// Get the channel specification (const).
    const IRecordSpecification& channelSpecification() const;

    /// Get the channel specification (to modify it).
    RecordSpecification& channelSpecification();
    *///

    /// Get the payload mode (const).
    const PayloadMode::Mode& payloadMode() const;

  private:

    // The default constructor is private (a payload specification is needed)
    FolderSpecification();

  private:

    /// The folder versioning mode.
    FolderVersioning::Mode m_versioningMode;

    /// The folder payload specification.
    RecordSpecification m_payloadSpec;

    /// The folder channel specification.
    //RecordSpecification m_channelSpec;

    /// The payload mode.
    PayloadMode::Mode m_payloadMode;

  };

}

#endif // COOLKERNEL_FOLDERSPECIFICATION_H
