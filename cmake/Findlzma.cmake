# - Locate lzmalibrary
# Defines:

#  LZMA_FOUND
#  LZMA_LIBRARIES
#  LZMA_LIBRARY_DIRS (not cached)

find_library(LZMA_LIBRARIES NAMES lzma)

get_filename_component(LZMA_LIBRARY_DIRS ${LZMA_LIBRARIES} PATH)

# handle the QUIETLY and REQUIRED arguments and set LZMA_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(lzma DEFAULT_MSG LZMA_LIBRARIES)

mark_as_advanced(LZMA_FOUND LZMA_LIBRARIES)
